import CartParser from './CartParser';

const expectResult = {
    "items": [
        {
            "id": "1234",
            "name": "Mollis consequat",
            "price": 9,
            "quantity": 2
        },
        {
            "id": "1234",
            "name": "Tvoluptatem",
            "price": 10.32,
            "quantity": 1
        },
        {
            "id": "1234",
            "name": "Scelerisque lacinia",
            "price": 18.9,
            "quantity": 1
        },
        {
            "id": "1234",
            "name": "Consectetur adipiscing",
            "price": 28.72,
            "quantity": 10
        },
        {
            "id": "1234",
            "name": "Condimentum aliquet",
            "price": 13.9,
            "quantity": 1
        }
    ],
    "total": 348.32
};
import * as uuid from 'uuid';

jest.mock('uuid');

let parser;
let validateString;
let notValidHeaderString;
let notValidBodyString;
let notValidTypeItemsInBody;
let errorHeaderObj;
let errorBodyObj;
let errorTypeItemsInBody;
let errorNegativeNumber;
let errorNonEmptyStringItem;

beforeEach(() => {
    parser = new CartParser();
    validateString = 'Product name,Price,Quantity\r\nMollis consequat,9.00,2\r\nTvoluptatem,10.32,1';
    notValidHeaderString = 'Product name,Price\r\nMollis consequat,9.00,2\r\nTvoluptatem,10.32,1';
    notValidBodyString = 'Product name,Price,Quantity\r\nMollis consequat,9.00\r\nTvoluptatem,10.32,1';
    notValidTypeItemsInBody = [
        'Product name,Price,Quantity\r\nMollis consequat,9.00,one\r\nTvoluptatem,10.32,1',
        'Product name,Price,Quantity\r\nMollis consequat,9.00,-2\r\nTvoluptatem,10.32,1',
        'Product name,Price,Quantity\r\n,9.00,2\r\nTvoluptatem,10.32,1'
    ];
    errorBodyObj = {
        "column": -1,
        "message": "Expected row to have 3 cells but received 2.",
        "row": 1,
        "type": "row"
    };
    errorHeaderObj = {
        "column": 2,
        "message": "Expected header to be named \"Quantity\" but received undefined.",
        "row": 0,
        "type": "header",
    };
    errorTypeItemsInBody = {
        type: 'cell',
        row: 1,
        column: 2,
        message: 'Expected cell to be a positive number but received "one".'
    };
    errorNegativeNumber = {
        type: 'cell',
        row: 1,
        column: 2,
        message: 'Expected cell to be a positive number but received "-2".'
    };
    errorNonEmptyStringItem = {
        type: 'cell',
        row: 1,
        column: 0,
        message: 'Expected cell to be a nonempty string but received "".'
    };

});
describe('CartParser - unit tests', () => {

	test('should return object cart item', () => {
		uuid.v4.mockReturnValue('1234');
		expect(parser.parseLine('name, 2, 2')).toEqual({
			name: 'name',
			price: 2,
			quantity: 2,
			id: '1234'
		})
	});

	test('should return total price from cartItems', () => {
		expect(parser.calcTotal([{ name: 'n', price: 9, quantity: 2, id: '2' },
			{ name: 'n', price: 5.50, quantity: 3, id: '1' }])).toBe(34.50)
	});

    test('should return empty array, validation error', () => {
        expect(parser.validate(validateString)).toEqual([])
    });

    test('should return error incorrect quantity of items in header', () => {
        expect(parser.validate(notValidHeaderString)[0]).toEqual(errorHeaderObj);
    });

    test('should return error incorrect quantity of items in body', () => {
        expect(parser.validate(notValidBodyString)[0]).toEqual(errorBodyObj);
    });

    test('should return error incorrect value of items in body', () => {
        expect(parser.validate(notValidTypeItemsInBody[0])[0]).toEqual(errorTypeItemsInBody);
        expect(parser.validate(notValidTypeItemsInBody[1])[0]).toEqual(errorNegativeNumber);
        expect(parser.validate(notValidTypeItemsInBody[2])[0]).toEqual(errorNonEmptyStringItem);
    });
});

describe('CartParser - integration test', () => {
	test('should return object with data', () => {
		uuid.v4.mockReturnValue('1234');
		expect(parser.parse('C:\\Users\\Ярослав\\PhpstormProjects\\BSA2021-Testing\\samples\\cart.csv')).toEqual(expectResult)
	});
});